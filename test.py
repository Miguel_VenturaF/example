%matplotlib inline
import pandas as pd
data = pd.read_csv('crypto_prices.csv', index_col='date', parse_dates=['date'])
data['SMA'] = data['btc'].rolling(50).mean()
data.plot(y=['btc','SMA'])
